package com.realdolmen.calculator;


import org.mariuszgromada.math.mxparser.Expression;

public class Calculator {
    public double calculate(String input) {
        Expression expression = new Expression(input);
        return expression.calculate();
    }
}
