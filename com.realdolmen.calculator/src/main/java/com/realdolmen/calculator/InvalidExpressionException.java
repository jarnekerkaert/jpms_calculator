package com.realdolmen.calculator;

public class InvalidExpressionException extends RuntimeException {

    public InvalidExpressionException() {
        super("Expression is invalid");
    }

    public InvalidExpressionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidExpressionException(String message) {
        super(message);
    }

    public InvalidExpressionException(Throwable cause) {
        super(cause);
    }
}
