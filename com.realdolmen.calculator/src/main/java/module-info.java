module com.realdolmen.calculator {
    exports com.realdolmen.calculator to com.realdolmen.main;
    requires org.junit.jupiter.api;
    requires MathParser.org.mXparser;
}