package com.realdolmen.calculator;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CalculatorTest {

    private static Calculator calculator;

    @BeforeAll
    static void beforeAll() {
        calculator = new Calculator();
    }

    @Test
    void givenEmptyString_ThrowInvalidExpressionException() {
        String input = "";
        double result = calculator.calculate(input);
        assertThat(Double.isNaN(result)).isTrue();
    }

    @Test
    void givenInvalidExpression_ThrowInvalidExpressionException() {
        String input = "abcdefg";
        double result = calculator.calculate(input);
        assertThat(Double.isNaN(result)).isTrue();
    }

    @Test
    void givenValidAddition_ReturnCorrectResult() {
        String input = "1+1";
        double result = calculator.calculate(input);
        assertThat(result).isEqualTo(2.0);
    }
}
