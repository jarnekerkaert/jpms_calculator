package com.realdolmen.main;

import com.realdolmen.calculator.Calculator;
import com.realdolmen.calculator.InvalidExpressionException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        try {
            while (!line.equalsIgnoreCase("quit")) {
                line = in.readLine();
                try {
                    double result = calculator.calculate(line);
                    System.out.println(result);
                } catch(InvalidExpressionException e) {
                    System.out.println(e.getMessage());
                }
            }
            in.close();
        } catch(IOException e) {
            System.out.println(e);
            System.out.println("Closing application...");
        }
    }
}
